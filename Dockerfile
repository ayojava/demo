FROM openjdk:11
MAINTAINER Ayodeji Ilori
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} demo.jar
ENTRYPOINT ["java","-jar","/demo.jar"]