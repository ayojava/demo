package org.javasoft.demo

import mu.KotlinLogging
import org.apache.commons.lang3.time.DateFormatUtils
import org.apache.commons.io.FileUtils
import org.springframework.amqp.rabbit.annotation.RabbitListener
import org.springframework.amqp.rabbit.core.RabbitTemplate
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.core.io.Resource
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Service
import org.springframework.util.ResourceUtils
import java.io.File
import java.nio.charset.StandardCharsets
import java.util.*


private val logger = KotlinLogging.logger {}

data class Message(val message: String ="MSG_", var increment : Int =0 ){

    fun displayImmediateMessage() : String{
        return "Got ".plus(message) + increment
    }

    fun displayObserverMessage(topicName : String) : String {
        val dateFormatStr = DateFormatUtils.format(Date(),"yyyy-MM-dd'T'hh:mm:ss.sssZ",TimeZone.getTimeZone("UTC"))
        return "\n ".plus(dateFormatStr).plus("  Topic ").plus(topicName).plus(": ").plus(message).plus(increment)
    }
}

@Service
class SchedulerService(@Autowired private val orig: Orig){

    @Scheduled(cron = "\${cron.format.value}")
    fun scheduleService(){
        orig.sendMessage()
    }
}

@Service
class Orig(@Autowired private val rabbitTemplate: RabbitTemplate , val rabbitMqConfig: RabbitMqConfig , val obse: Obse) {

    var counter = 1;

    fun sendMessage(){
        if(counter < 4){
            val message = Message(increment = counter)
            rabbitTemplate.convertAndSend(rabbitMqConfig.exchangeName,rabbitMqConfig.routingKey1,message)
        }
        if(counter == 4){
            obse.writeContentToFile()
        }
        counter++
    }
}

@Service
class Imed(@Autowired val rabbitMqConfig: RabbitMqConfig, private val rabbitTemplate: RabbitTemplate){

    @RabbitListener(queues = ["\${rmq.queue1}"])
    fun receiveMessage(message: Message){
        Thread.sleep(1000)
        logger.info { "${message.displayImmediateMessage()}"  }
        sendMessage(message)
    }

    fun sendMessage(message: Message){
        rabbitTemplate.convertAndSend(rabbitMqConfig.exchangeName,rabbitMqConfig.routingKey2,message)
    }

}

@Service
class Obse(@Autowired val rabbitMqConfig: RabbitMqConfig ,@Autowired val startUpUtil: StartUpUtil){

    var content: String = ""

    @RabbitListener(queues = ["\${rmq.queue1}"],containerFactory="jsaFactory")
    fun receiveMessage1(message: Message){
        logger.info { "${message.displayObserverMessage(rabbitMqConfig.queue1)}"  }
        content = content.plus(message.displayObserverMessage(rabbitMqConfig.queue1))
        }

    @RabbitListener(queues = ["\${rmq.queue2}"],containerFactory="jsaFactory")
    fun receiveMessage2(message: Message){
        logger.info { "${message.displayObserverMessage(rabbitMqConfig.queue2)}"  }
        content = content.plus(message.displayObserverMessage(rabbitMqConfig.queue2))
    }

    fun writeContentToFile(){
        val resolvePath = startUpUtil.filePath
        val file: File = File(resolvePath);
        FileUtils.write(file,content, StandardCharsets.UTF_8,false )
    }
}

