package org.javasoft.demo

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.ConfigurationPropertiesScan
import org.springframework.boot.runApplication
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.scheduling.annotation.EnableScheduling
import org.springframework.util.ResourceUtils
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.io.File
import java.nio.file.Files

@EnableScheduling
@SpringBootApplication
@ConfigurationPropertiesScan
class DemoApplication

fun main(args: Array<String>) {
    runApplication<DemoApplication>(*args)
}


@RestController
@RequestMapping("/")
class MessageController(@Autowired val startUpUtil: StartUpUtil ){

    @GetMapping
    fun getFileContents() : ResponseEntity<String> {
        val resolvePath = startUpUtil.filePath
        val file: File = File(resolvePath);
        return ResponseEntity(String(Files.readAllBytes(file.toPath())), HttpStatus.OK);
    }
}

