package org.javasoft.demo

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import org.apache.commons.io.FileUtils
import org.apache.commons.io.IOUtils
import org.apache.commons.lang3.StringUtils
import org.springframework.amqp.core.Binding
import org.springframework.amqp.core.BindingBuilder
import org.springframework.amqp.core.Queue
import org.springframework.amqp.core.TopicExchange
import org.springframework.amqp.rabbit.config.SimpleRabbitListenerContainerFactory
import org.springframework.amqp.rabbit.connection.ConnectionFactory
import org.springframework.amqp.rabbit.core.RabbitTemplate
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter
import org.springframework.amqp.support.converter.MessageConverter
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.amqp.SimpleRabbitListenerContainerFactoryConfigurer
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.core.io.ResourceLoader
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler
import org.springframework.stereotype.Component
import java.io.File
import java.io.FileNotFoundException
import java.io.FileOutputStream
import javax.annotation.PostConstruct


@ConstructorBinding
@ConfigurationProperties(prefix = "rmq")
data class RabbitMqConfig (val exchangeName : String , val queue1 : String , val queue2 :String, val routingKey1 : String,val routingKey2 : String)


@Configuration
class BeanConfig (@Autowired val rabbitMqConfig: RabbitMqConfig){

    @Bean
    fun topicExchange(): TopicExchange {
        return TopicExchange(rabbitMqConfig.exchangeName , false , false)
    }

    @Bean
    fun jsonMessageConverter(): MessageConverter {
        return Jackson2JsonMessageConverter()
    }

    @Bean
    fun rabbitTemplate(connectionFactory: ConnectionFactory , topicExchange: TopicExchange): RabbitTemplate {
        val rabbitTemplate = RabbitTemplate(connectionFactory)
       // rabbitTemplate.exchange = topicExchange.name
        rabbitTemplate.messageConverter = jsonMessageConverter()

        return rabbitTemplate
    }

    @Bean
    fun objectMapper(): ObjectMapper{
        val objectMapper = ObjectMapper()
        objectMapper.registerKotlinModule()
        return objectMapper
    }
    @Bean
    fun queue1(): Queue {
        return Queue(rabbitMqConfig.queue1, false)
    }

    @Bean
    fun queue2(): Queue {
        return Queue(rabbitMqConfig.queue2, false)
    }

    @Bean
    fun binding1(exchange: TopicExchange): Binding {
        return BindingBuilder.bind(queue1()).to(exchange).with(rabbitMqConfig.routingKey1)
    }

    @Bean
    fun binding2(exchange: TopicExchange): Binding {
        return BindingBuilder.bind(queue2()).to(exchange).with(rabbitMqConfig.routingKey1)
    }

    @Bean
    fun jsaFactory(
        connectionFactory: ConnectionFactory?,
        configurer: SimpleRabbitListenerContainerFactoryConfigurer
    ): SimpleRabbitListenerContainerFactory? {
        val factory = SimpleRabbitListenerContainerFactory()
        configurer.configure(factory, connectionFactory)
        factory.setMessageConverter(jsonMessageConverter())
        return factory
    }


    @Bean
    fun threadPoolTaskScheduler(): ThreadPoolTaskScheduler? {
        val threadPoolTaskScheduler = ThreadPoolTaskScheduler()
        threadPoolTaskScheduler.poolSize = 5
        threadPoolTaskScheduler.setThreadNamePrefix("ThreadPoolTaskScheduler")
        return threadPoolTaskScheduler
    }
}

@Component
class StartUpUtil(){

    var filePath :String = ""
    @PostConstruct
    fun doInit(){
        val fileUtil = FileUtil()
        val fileToDelete: File = File(fileUtil.resolvePath("log.txt"));
        filePath = fileToDelete.absolutePath
        FileUtils.deleteQuietly(fileToDelete);
        FileUtils.touch(File(filePath));
    }
}

class FileUtil{
    fun resolvePath(path: String): String? {
        try {
            if (File(path).exists()) {
                return path
            }
            val `is` = javaClass.classLoader.getResourceAsStream(path) ?: throw FileNotFoundException("Could not load text file")
            val oFile = File.createTempFile(StringUtils.replacePattern(path, "[^a-zA-Z0-9]+", "_"), ".tmp")
            oFile.deleteOnExit()
            IOUtils.copy(`is`, FileOutputStream(oFile))
            return oFile.absolutePath
        } catch (ex: Exception) {
            println(ex.message)
        }
        return path
    }
}